<?php

namespace libs\http;

class Response{
    
    public function __construct() {
        
    }
    
    public function send( $response ){
        
        header("HTTP/1.0 200 OK");
        
        echo $response;
    }   
    
    public function sendResponse404() {

        header("HTTP/1.0 404 Not Found");
        echo "<h1>404 Not Found</h1>";
        echo "The page that you have requested could not be found.";
        exit();
    }
    
}

?>
