<?php

namespace libs\http;

class Request {

    public $post;
    public $get;
    public $request_method;
    public $current_script;
    public $user_ip;
    public $server_name;
    public $server_protocol;
    public $query_string;
    public $document_root;
    public $http_host;
    public $absolute_base_url;
    public $uri;

    public function __construct() {

        $this->post = $_POST;

        $this->get = $_GET;

        $this->request_method = $_SERVER['REQUEST_METHOD'];

        $this->current_script = $_SERVER['PHP_SELF'];

        $this->user_ip = $_SERVER['SERVER_ADDR'];

        $this->server_name = $_SERVER['SERVER_NAME'];

        $this->absolute_base_url = 'http://' . $this->server_name;

        $this->server_protocol = $_SERVER['SERVER_PROTOCOL'];

        $this->query_string = $_SERVER['QUERY_STRING'];

        $this->document_root = $_SERVER['DOCUMENT_ROOT'];

        $this->http_host = $_SERVER['HTTP_HOST'];
        
        $this->uri = $_SERVER['REQUEST_URI'];
    }

    public function getUri() {

        $uri = substr($this->uri, 0, strpos($this->uri, '?') !== false ? strpos($this->uri, '?') : strlen($this->uri));

        return $uri;
    }

    public function getMethod() {

        return $this->request_method;
    }

    public function isAjax() {

        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            return true;
        } else {
            return false;
        }
    }

    public function isPost() {

        if ($this->getMethod() == 'POST') {
            return true;
        }

        return false;
    }

}

?>