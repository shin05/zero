<?php

$router = new controllers\Router();

$routes = array(
    
    '/|/index|/home' => array(
        'method' => 'GET|POST',
        'response' => 'Home', // if is index method, just put the controller name
        'name' => 'Home'
    ),
    '/user/{str:name}/{int:id}/{aln:dni}/{str:city}' => array(
        'method' => 'GET',
        'response' => 'Account@sayMeName',
        'name' => 'say-my-name',
        'filter' => 'auth'
    ),
    '/mi-cuenta' => array(
        'method' => 'GET|POST', // GET or POST method !
        'response' => 'Account',
        'name' => 'say-my-name',
    ),
    '/getpage' => array(
        'method' => 'GET',
        'response' => function() {
            echo "Ajax!";
        },
        'name' => 'call-ajax'
    )
);

$router->get('/fuyu', array(
    'response' => 'Home@fuyu',
    'filter' => 'cold|fuyu',
    'name' => 'fuyuuu',
    'after' => function() {
        echo "<p>After says: This is after Closure of fuyu route!</p>"; // excute ir after
    },
    'before' => function() {
        echo "<p>Before says: This is before Closure of fuyu route!</p>"; // excute ir before
}));

$router->register($routes);