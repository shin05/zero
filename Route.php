<?php

namespace controllers;

use libs\http\Request;
use libs\Filters;

class Route {

    protected $pattern;
    protected $request_method;
    protected $callable;
    protected $name;
    protected $params;
    protected $controller;
    protected $method_controller;
    protected $after;
    protected $before;
    protected $filters;
    protected $uri;

    public function __construct($pattern, $callable, $method) {

        if (is_callable($callable)) {

            $this->callable = $callable;
        } else {

            $conttmethod = explode('@', $callable);

            if (count($conttmethod) == 1) {
                $conttmethod[1] = 'index';
            }

            $this->controller = $conttmethod[0];
            $this->method_controller = $conttmethod[1];
        }

        $request = new Request();

        $this->uri = $request->getUri();

        $this->pattern = $pattern;

        $this->request_method = $method;

        $this->params = $this->getParams($pattern);
    }

    public function getParams($pattern) {

        $uri = explode('/', $this->uri);

        $patterns = explode('/', $pattern);

        return array_diff($uri, $patterns);
    }

    public function before($callable) {

        $this->before = $callable;
    }

    public function after($callable) {

        $this->after = $callable;
    }

    public function executeFilters($filter, $filter_name) {

        if (method_exists($filter, $filter_name)) {

            return call_user_func_array(array($filter, $filter_name), $this->params);
        }
    }

    public function execute() {

        if (!empty($this->filters)) {

            $filters = explode('|', $this->filters);

            $filter = new Filters();

            foreach ($filters as $filter_name) {

                $this->params[] = $this->executeFilters($filter, $filter_name);
            }
        }
        
        if (is_callable($this->after)) {
            call_user_func_array($this->after, $this->params);
        }

        if (is_callable($this->callable)) {
            call_user_func_array($this->callable, $this->params);
        } else {
            $this->isController();
        }

        if (is_callable($this->before)) {
            call_user_func_array($this->before, $this->params);
        }
    }

    public function isController() {

        $sufix_c = 'Controller';
        $sufix_m = 'Action';
        $namespace = 'controllers\\';

        $controller = $namespace . $this->controller . $sufix_c; // add sufix controller
        $method = $this->method_controller . $sufix_m; // add sufix method

        $c = new $controller();
        call_user_func_array(array($c, $method), $this->params);
    }

    public function name($name) {

        $this->name = $name;
    }

    public function getName() {

        return $this->name;
    }

    public function getPattern() {

        return $this->pattern;
    }

    public function setFilter($filter) {

        $this->filters = $filter;
    }

}

?>