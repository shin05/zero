<?php

namespace controllers;

use libs\http\Request;
use libs\http\Response;

class Router {

    protected $request;
    protected $request_method;
    protected $wildcards = array(
        'any' => '(.+)',
        'aln' => '([[:alnum:]]+)',
        'int' => '([\d]+)',
        'str' => '[A-Za-z\-]+',
        'seg' => '([^/]*)',
    );
    protected $base_url;
    protected $response;
    protected $routes = array(
        'ANY' => array(),
        'GET' => array(),
        'POST' => array(),
        'PUT' => array(),
        'DELETE' => array()
    );

    public function __construct() {

        $this->request = new Request();
        $this->response = new Response();

        $this->request_method = $this->request->getMethod();

        $this->base_url = $this->request->http_host;
    }

    /**
     * Create a route via GET method.
     * 
     * @var string $pattern /user/{s:name}/{int:id}.
     * @var string|closure $action 'Home@index' or Closure.
     */
    public function get() {

        $args = func_get_args();

        $pattern = $args[0];

        $route = array(
            $pattern => array(
                'method' => 'GET'
            )
        );

        $properties = $args[1];

        if (is_callable($properties)) {

            $route[$pattern]['response'] = $properties;
        } elseif (is_array($properties)) {

            $route[$pattern]['response'] = $properties['response'];
            
            if( isset($properties['filter']) ){
                $route[$pattern]['filter'] = $properties['filter'];
            }
            if( isset($properties['name']) ){
                $route[$pattern]['name'] = $properties['name'];
            }
            if( isset($properties['after']) ){
                $route[$pattern]['after'] = $properties['after'];
            }
            if( isset($properties['before']) ){
                $route[$pattern]['before'] = $properties['before'];
            }
        }
        
        $this->register($route);
    }

    /**
     * Register all routes
     * 
     * @var array $routes
     */
    public function register($routes) {

        foreach ($routes as $pattern => $properties) {

            $this->createRoute($pattern, $properties);
        }
    }

    /**
     * Create the Route.
     * 
     * @var string $patter
     * @var arra $properties
     * 
     * return Route $route.
     */
    public function createRoute($pattern, $properties) {

        $response = $properties['response']; // the callback|controller@method

        $route = new Route($pattern, $response, $properties['method']);

        if (!empty($properties['name'])) {
            $route->name($properties['name']);
        }
        if (!empty($properties['filter'])) {
            $route->setFilter($properties['filter']);
        }
        if (!empty($properties['before'])) {
            $route->before($properties['before']);
        }
        if (!empty($properties['after'])) {
            $route->after($properties['after']);
        }

        $methods = explode('|', $properties['method']);

        if (count($methods) == 2) {

            foreach ($methods as $method) {

                $this->routes[$method][$pattern] = $route;
            }
            return;
        }

        $this->routes[$properties['method']][$pattern] = $route;
    }

    /**
     * For each route of this request method try to make match
     * with the current uri, if make match, execute the route.
     * Else, send 404.
     */
    public function run() {
        
        foreach ($this->routes[$this->request_method] as $pattern => $route) {

            if (strstr($pattern, '|')) {

                $patterns = explode('|', $pattern);

                foreach ($patterns as $pattern) {
                    if ($this->returnRoute($pattern)) {
                        return $route->execute();
                    }
                }
            } else {
                if ($this->returnRoute($pattern)) {
                    return $route->execute();
                }
            }
        }

        $this->response->sendResponse404();
    }

    /**
     * Create a regex by the pattern and then try to match with the uri.       
     * 
     * @var string $pattern /user/{s:name}/{int:id}.
     * 
     * return bool.
     */
    public function returnRoute($pattern) {

        if ($pattern == $this->request->getUri()) // if is true, is the index ( '/' )
            return true;

        $patterns = explode('/', $pattern); // explode the pattern with no_wilds and wildcards

        array_shift($patterns); // remove the first empty element

        $matches_no_wilds = preg_grep('/^\w/', $patterns); // no_wild ( /user )
        $matches_wild = preg_grep('/[^\w]/', $patterns); //wildcards ( {s:name} )

        $regex = ''; // initialize the regular expression

        preg_replace_callback('/{[a-z]+:/', function($match) use(&$regex) { // create the regex,
            $type = str_replace(':', '', str_replace('{', '', $match));

            foreach ($type as $index) {

                $regex .= '#' . $this->wildcards[$index];
            }
        }, $matches_wild);

        $regex_final = str_replace('#', '/', $regex);

        $no_wilds = '';

        foreach ($matches_no_wilds as $no_wild) { // create a first part of regex with no_wilds ( /user/account )
            $no_wilds .= '/' . $no_wild;
        }

        $match_route = array();

        preg_match('{' . $no_wilds . $regex_final . '$}', $this->request->getUri(), $match_route);


        if (!empty($match_route))
            if ($match_route[0] == $this->request->getUri()) {
                return true;
            } else {
                return false;
            }
    }

    /**
     * Add a wildcard type for validate the route pattern parameters.
     */
    public function addWildcard($key, $value) {

        $this->wildcards[$key] = $value;
    }

    /**
     * Redirect to Route
     */
    public function redirect($name) {

        foreach ($this->routes['GET'] as $route) {

            if ($route->getName() == $name) {

                $route->execute();
                exit();
            }
        }
    }

    public function __destruct() {
        $this->run();
    }

}

?>
