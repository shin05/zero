<?php

namespace libs;

class Filters {

    public function auth($name, $id) {

        $users = array(
            array(
                'id' => 1,
                'name' => 'shin',
                'avatar' => 'shin',
                'about' => 'I\'am shin, I don\'t know what I should say... This is me!'
            ),
            array(
                'id' => 2,
                'name' => 'gumi',
                'avatar' => 'gumi',
                'about' => 'Watashi wa Gumi daaa! Vocaloid daaa!'
            )
        );

        foreach ($users as $user) {

            if ($user['name'] == $name && $user['id'] == $id) {

                return $user;
            }
        }

        return false;
    }

    public function fuyu() {

        echo "<p>Another filter says: It's winter!</p>";
    }

    public function cold() {

        echo "<p>Filter says: It's so cold!</p>";
    }

}

?>
